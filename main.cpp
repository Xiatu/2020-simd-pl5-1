#include <errno.h>
#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <time.h>
#include <immintrin.h>

using namespace cimg_library;


#define CUSTOM_SIZE 256

typedef double data_t;

//Funcion minimo
data_t min_simd(data_t *values, uint limit)
{
	data_t minimum = 255;
	uint step = CUSTOM_SIZE/(sizeof(data_t)*8);
	__m256d vTemp;
	__m256d min_val = _mm256_set1_pd(255);
	
	//Calcular los 4 valores minimos
	for(uint i = 0; i < limit; i = i + step) {
		vTemp = _mm256_loadu_pd (values + i);
		min_val = _mm256_min_pd(vTemp,min_val);
	}
	//Calcular el valor minimo de los 4
	for(uint i = 0; i < step; i++){
		if(min_val[i] < minimum){
		minimum = min_val[i];
		}
	}
	
	return minimum;
	
}

//Funcion maximo
data_t max_simd(data_t *values, uint limit)
{
	data_t maximum = 0;
	uint step = CUSTOM_SIZE/(sizeof(data_t)*8);
	__m256d vTemp;
	__m256d max_val = _mm256_set1_pd(0);
	
	// Calcular los 4 valores maximos
	for(uint i = 0; i < limit; i = i + step) {
		vTemp = _mm256_loadu_pd (values + i);
		max_val = _mm256_max_pd(vTemp,max_val);
	}
	// Calcular el valor maximo de los 4
	for(uint i = 0; i < step; i++){
		if(max_val[i] > maximum)
			maximum = max_val[i];
	}
	
	return maximum;
	
}

const char* SOURCE_IMG = "uniovi_2_ev-2.bmp";
const char* DESTINATION_IMG = "result.bmp";
const uint repmax = 20;
int main() {
	try{
	// Ope file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointer to the R, G and B components
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; //Pointer to the new image pixels
	uint width, height; // Width and height of the image
	uint nComp; // Number of image components
	uint size = CUSTOM_SIZE / (sizeof(data_t)*8);
	uint index;
	
	
	/***************************************************
	 * TODO : Variables initialization.
	 *	 - Prepare variables for the algorithm
	 *	 - This is not included in the benchmark time
	 */
	 
	 struct timespec tStart, tEnd;
	 double dElapsedTimeS;
	 
	 srcImage.display(); // Displays the source image 
	 width = srcImage.width(); // Getting information from the source image
	 height = srcImage.height(); 
	 nComp = srcImage.spectrum(); // source image number of components	
	
	
	
	pDstImage = (data_t *) _mm_malloc (width * height * nComp * sizeof(data_t), sizeof(__m256));
	if(pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}
	
	// Pointers to the component arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array 
	pGsrc = pRsrc + height * width; // pGcom pointes to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array
	
	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;
	
	
	
	/************************************************
	 TODO: Algorithm start.
	 *	 - Measure initial time
	 */
	 
	if (clock_gettime(CLOCK_REALTIME,&tStart))
	{
		printf("ERROR: clock_gettime: %d.\n",errno);
		exit(EXIT_FAILURE);
	}
	
	/************************************************
	 * FIXME: Algorithm
	 * In this example, the algorithm is a components swap
	 * 
	 * TO BE REPLACED BY YOUR ALGORITHM
	 */
	
	for (uint rep = 0; rep < repmax ;rep++){
		
		__m256d Rmin = _mm256_set1_pd(min_simd(pRsrc, height * width));
		__m256d Gmin = _mm256_set1_pd(min_simd(pGsrc, height * width));
		__m256d Bmin = _mm256_set1_pd(min_simd(pBsrc, height * width));
	
		__m256d Rmax = _mm256_set1_pd(max_simd(pRsrc, height * width));
		__m256d Gmax = _mm256_set1_pd(max_simd(pGsrc, height * width));
		__m256d Bmax = _mm256_set1_pd(max_simd(pBsrc, height * width));
		
		if (width % size !=0){
			printf("ERROR DE TAMAÑO: LA ANCHURA NO ES MULTIPLO DE %d.",size);
		}
		__m256d vConst = _mm256_set1_pd(255); 
		__m256d result;
		__m256d vComp;
		
		
		for (uint i = 0; i < height; i++){
			for(uint j = 0; j < width; j = j + size){
				index = i * width + j;
				
				//Rojo
				vComp = _mm256_loadu_pd((pRsrc+index));
				result = _mm256_mul_pd (_mm256_div_pd(_mm256_sub_pd(vComp, Rmin),_mm256_sub_pd (Rmax, Rmin)), vConst);
				_mm256_storeu_pd ((pRdest+index),result);
				
				//Verde
				vComp = _mm256_loadu_pd((pGsrc+index));
				result = _mm256_mul_pd (_mm256_div_pd(_mm256_sub_pd(vComp, Gmin),_mm256_sub_pd (Gmax, Gmin)), vConst);
				_mm256_storeu_pd ((pGdest+index),result);
				
				//Azul
				vComp = _mm256_loadu_pd((pBsrc+index));
				result = _mm256_mul_pd (_mm256_div_pd(_mm256_sub_pd(vComp, Bmin),_mm256_sub_pd (Bmax, Bmin)), vConst);
				_mm256_storeu_pd ((pBdest+index),result);
				
			
				
			}
			
			
			
			
		}
	}
	
	/**************************************************
	 * TODO: End of the algorithm.
	 * 	 - Measure the end time
	 *	 - Calculate the elapsed time
	 */
	 
	if (clock_gettime(CLOCK_REALTIME, &tEnd)){
		printf("ERROR: clock_gettime: %d.\n",errno);
		exit(EXIT_FAILURE);
		}
	
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
		dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
		

			
		// Create a new image object with the calculated pixels
		// In case of normal color images use nComp=3,
		// In case of B/W images use nComp=1.
		CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

		// Store destination image in disk
		dstImage.save(DESTINATION_IMG); 

		// Display destination image
		dstImage.display();
		
		printf("\nTiempo que tarda en ejecutar: %f\n",dElapsedTimeS);
		free(pDstImage);
		}
		catch (CImgIOException e) {    
			printf("\tERROR IOEXCEPTION.\n");
		}

		return 0;
		    
}